'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');
var builder = require('xmlbuilder');
const jsf = require('json-schema-faker');
const chance = require('chance')
const faker = require('faker')
jsf.extend('chance', () => new chance.Chance());
jsf.extend('faker', () => faker);

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */
module.exports = {
  listUser: getUser,
  listGraph: getGraph,
  stats: getStats
};

/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
function getUser(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  var user = { 
                  "id": `${faker.random.number()}`,
                  "takeOffId": `${faker.random.number()}`, 
                  "name": faker.name.findName(), 
                  "photo": faker.image.people(),
                  "status": "student"
              }
  res.json([user]);
}

function getGraph(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  // var name = req.swagger.params.tea.value || 'white';
  var graphs = { 
      "theme": faker.random.word(), 
      "owner": faker.name.findName(), 
      "graphTitles": ["math", "phys"],
      "graphsLink": ["a","b"]
    }
  res.json([graphs]);
}


function getStats(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  // var name = req.swagger.params.tea.value || 'white';
  var doc = builder.create('graphs')
    .ele('graph')
      .att('theme', 'math')
      .att('owner', 'teacher')
      .att('date', `${faker.date.recent()}`)
      .up()
    .ele('graph')
      .att('theme', 'physic')
      .att('owner', 'me')
      .att('date', `${faker.date.recent()}`)
      .up() 
  .end({ pretty: true });

  res.type('application/xml');
  res.set('x-template', 'orders.xsl')
  res.send(doc.toString());
}
