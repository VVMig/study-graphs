function burgerMenu() {
  let btn = document.querySelector('.header__burger-menu');

  btn.addEventListener('click', toggleMenu);
}

function toggleMenu(param) { 
  let menu = document.querySelector('.header__menu');
  let btn = document.querySelector('.header__burger-menu');

  btn.classList.toggle('header__burger-menu-active');
  btn.classList.toggle('header__burger-menu-disactive');

  if(menu.classList[1] === 'header__menu-disactive') {
    menu.style.display = 'block';
  }

  menu.classList.toggle('header__menu-active');
  menu.classList.toggle('header__menu-disactive');

  menu.addEventListener('animationend',(e)=>{
    if(menu.classList[1] === 'header__menu-disactive') {
      menu.style.display = 'none';
    }
    else if(menu.classList[1] === 'header__menu-active') {
      menu.style.display = 'block';
    }
  })

}

burgerMenu();